#!/usr/bin/env python3
# Borrowed from
# https://gist.github.com/1kastner/e083f9e813c0464e6a2ec8910553e632
# Reflects the requests from HTTP methods GET, POST, PUT, and DELETE
# Written by Nathan Hamiel (2010)

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import parse_qs, urlparse
from optparse import OptionParser

import socket


class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):

        request_path = self.path
        params = parse_qs(urlparse(request_path).query)
        room = 'not-set'
        if 'room' in params:
            room = params['room'][0]
        hostname = socket.gethostname()
        path = request_path.split('?')[0]

        message = f"{hostname};{path};{room}\n"

        self.send_response(200)
        self.send_header("Set-Cookie", "foo=bar")
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(bytes(message, 'utf-8'))

    do_POST = do_GET
    do_PUT = do_GET
    do_DELETE = do_GET


def main():
    port = 8080
    print('Listening on 0.0.0.0:%s' % port)
    server = HTTPServer(('', port), RequestHandler)
    server.serve_forever()


if __name__ == "__main__":
    parser = OptionParser()
    parser.usage = ("Creates an http-server that will echo out any GET "
                    "or POST parameters\n"
                    "Run:\n\n"
                    "   reflect")
    (options, args) = parser.parse_args()

    main()
